var waitGetSession = false;

function getSessions() {
	if (!waitGetSession) {
		waitGetSession             = true;
		var xmlhttp                = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function () {
			if (xmlhttp.readyState === 4) {
				waitGetSession = false;
			}
		}
		xmlhttp.open(
			"GET",
			"sync?time=" + (new Date()).getTime(),
			true
		);
		xmlhttp.send();
	}
}

setInterval(getSessions, 1000 * 2);
