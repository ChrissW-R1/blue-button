package me.chrisswr1.bluebutton;

import org.jetbrains.annotations.NotNull;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * {@link HttpServlet}, which merges the request methods GET and POST
 *
 * @version 1.0.0
 * @since 1.0.0
 */
public abstract class BaseServlet
extends HttpServlet {
	/**
	 * Handles a request. Independent to the method (GET or POST).
	 *
	 * @param request  the {@link HttpServletRequest} from the client
	 * @param response the {@link HttpServletResponse},
	 *                 which will be sent to the client
	 * @throws IOException if any in- or output {@link Exception} occurred
	 *
	 * @see BaseServlet#doGet(HttpServletRequest, HttpServletResponse)
	 * @see BaseServlet#doPost(HttpServletRequest, HttpServletResponse)
	 *
	 * @since 1.0.0
	 */
	protected abstract void doRequest(
		final @NotNull HttpServletRequest request,
		final @NotNull HttpServletResponse response
	)
	throws IOException;

	@Override
	protected void doGet(
		final @NotNull HttpServletRequest request,
		final @NotNull HttpServletResponse response
	)
	throws IOException {
		this.doRequest(request, response);
	}

	@Override
	protected void doPost(
		final @NotNull HttpServletRequest request,
		final @NotNull HttpServletResponse response
	)
	throws IOException {
		this.doRequest(request, response);
	}
}
