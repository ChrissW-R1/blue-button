package me.chrisswr1.bluebutton.quiz;

import lombok.Getter;
import me.chrisswr1.bluebutton.BaseServlet;
import me.chrisswr1.bluebutton.sync.TimeSync;
import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Servlet to handle buzzers
 *
 * @version 1.0.0
 * @since 1.0.0
 */
@WebServlet(urlPatterns = "/buzzer")
public class BuzzerServlet
extends BaseServlet {
	/**
	 * serial version unique identifier
	 *
	 * @since 1.0.0
	 */
	@Getter
	private static final long serialVersionUID = 1;

	@Override
	protected void doRequest(
		final @NotNull HttpServletRequest request,
		final @NotNull HttpServletResponse response
	) throws IOException {
		HttpSession session  = request.getSession();
		DateTime    buzzTime = DateTime.now().minus(
			TimeSync.getTimeDiff(session)
		);

		response.getWriter().println(Buzzer.buzz(
			request.getSession(),
			buzzTime
		));
	}
}
