package me.chrisswr1.bluebutton.sync;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * organize synchronized {@link HttpSession}s
 *
 * @version 1.0.0
 * @since 1.0.0
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TimeSync {
	/**
	 * {@link Map} of all synchronized sessions
	 *
	 * @since 1.0.0
	 */
	private static final Map<HttpSession, Long> syncSessions =
		new ConcurrentHashMap<>();

	/**
	 * gives of all synchronized {@link HttpSession}s
	 *
	 * @return a {@link Map} of all synchronized {@link HttpSession}s
	 *
	 * @since 1.0.0
	 */
	public static Map<HttpSession, Long> getSyncSessions() {
		return new HashMap<>(TimeSync.syncSessions);
	}

	/**
	 * gives the time difference of a {@link HttpSession}
	 *
	 * @param session the {@link HttpSession} to get the time difference of
	 * @return the time difference of the given session
	 *
	 * @since 1.0.0
	 */
	public static long getTimeDiff(
		final @NotNull HttpSession session
	) {
		return TimeSync.syncSessions.get(session);
	}

	/**
	 * adds a {@link HttpSession} to synchronize it
	 *
	 * @param session  the {@link HttpSession} to add
	 * @param timeDiff the time difference of the session
	 *
	 * @since 1.0.0
	 */
	public static void addSession(
		final @NotNull HttpSession session,
		final long timeDiff
	) {
		TimeSync.syncSessions.put(session, timeDiff);
	}

	/**
	 * removes a {@link HttpSession} from the store of synchronized sessions
	 *
	 * @param session the {@link HttpSession} to remove
	 *
	 * @since 1.0.0
	 */
	public static void removeSession(
		final @NotNull HttpSession session
	) {
		TimeSync.syncSessions.remove(session);
	}
}
