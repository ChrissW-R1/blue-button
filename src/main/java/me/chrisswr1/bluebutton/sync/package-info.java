/**
 * package for synchronize web sessions
 *
 * @version 1.0.0
 * @since 1.0.0
 */
package me.chrisswr1.bluebutton.sync;
