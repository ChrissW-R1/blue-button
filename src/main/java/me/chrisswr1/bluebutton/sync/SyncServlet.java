package me.chrisswr1.bluebutton.sync;

import lombok.Getter;
import me.chrisswr1.bluebutton.BaseServlet;
import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map.Entry;

/**
 * {@link Servlet} to synchronize {@link HttpSession}s
 *
 * @version 1.0.0
 * @since 1.0.0
 */
@WebServlet(urlPatterns = {"/sync"})
public class SyncServlet
extends BaseServlet {
	/**
	 * serial version unique identifier
	 *
	 * @since 1.0.0
	 */
	@Getter
	private static final long serialVersionUID = 1;

	@Override
	protected void doRequest(
		final @NotNull HttpServletRequest request,
		final @NotNull HttpServletResponse response
	)
	throws IOException {
		if (!(request.getParameterMap().containsKey("time"))) {
			throw new IOException("No time parameter given!");
		}

		try {
			HttpSession session     = request.getSession();
			long        sendTime    = Long.parseLong(request.getParameter(
				"time"
			));
			DateTime    receiveTime = DateTime.now();
			long        timeDiff    = receiveTime.getMillis() - sendTime;

			TimeSync.addSession(session, timeDiff);

			StringBuilder sessionList = new StringBuilder();
			sessionList.append(
				"<table><tr><th>Session</th><th>Time difference</th></tr>"
			);
			for (
				Entry<HttpSession, Long> syncSession :
				TimeSync.getSyncSessions().entrySet()
			) {
				String sessionId = syncSession.getKey().getId();
				sessionList.append("<tr><td");
				if (sessionId.equalsIgnoreCase(request.getSession().getId())) {
					sessionList.append(" bgcolor=\"green\"");
				}
				sessionList.append(">");
				sessionList.append(sessionId);
				sessionList.append("</td><td");
				if (sessionId.equalsIgnoreCase(request.getSession().getId())) {
					sessionList.append(" bgcolor=\"green\"");
				}
				sessionList.append(">");
				sessionList.append(syncSession.getValue());
				sessionList.append("</td></tr>");
			}
			sessionList.append("</table>");

			ServletContext context = request.getServletContext();
			context.setAttribute("sessions", sessionList.toString());
		} catch (NumberFormatException e) {
			throw new IOException("Couldn't parse time parameter!", e);
		}
	}
}
